﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace TestBinding
{
    public class TestBindingViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _labelOpacity { get; set; }
        private string _labelText { get; set; }

        public TestBindingViewModel()
        {
            LabelText = "Fudge";
            LabelOpacity = 1;

            ChangeOpacity = new Command(() =>
            {
                LabelOpacity = LabelOpacity == 0 ? 1 : 0;
            });

            SetText = new Command(() =>
            {
                LabelText = LabelText.Equals("Fudge") ? "DoubleFudge" : "Fudge";
            });
        }

        public int LabelOpacity
        {
            protected set
            {
                _labelOpacity = value;
                OnPropertyChanged("_labelOpacity");
            }
            get { return _labelOpacity; }
        }

        public string LabelText
        {
            protected set
            {
                _labelText = value;
                OnPropertyChanged("_labelText");
            }
            get { return _labelText; }
        }

        #region ICommand Implementations
        public ICommand ChangeOpacity { protected set; get; }
        public ICommand SetText { protected set; get; }
        #endregion

        #region OnPropertyChanged Implementation
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            Console.WriteLine($"Property Changed: {propertyName}");
        }
        #endregion
    }
}
